
Installation Instructions:

1. Copy the zoho_v2api module directory into your modules directory.

2. Enable zoho_v2api module at: Administer > Modules (admin/modules)

3. Go to Administer > People > Permissions (admin/people/permissions) 
   and grant 'Administer zoho_v2api module' permission to the needed user roles.

4. Configure Zoho CRM account settings at admin/config/services/zoho_v2api
4.1 Use this only for the first time to authenticate your application and to generate a new grant_token.
Grant token is a short-lived token (valid only for a minute) and will be used to generate the access
token and refresh token automatically.
4.2 Before you get started with authorization, you need to register your application with Zoho CRM API.
Go to the site Zoho developer console (http://accounts.zoho.com/developerconsole) and add client.
You will receive Client id, Client Secret, Redirect URI.
Redirect URI you can see in comments on the admin/config/services/zoho_v2api page.

5. For test purpose you can work with your free Zoho account.

6. Pay attention, Zoho Layouts field have length limits. For instance, 255 chars for text field. If you send field with more than
255 chars length or with wrong field name, you will get error.

NOTE! This module version can work with only 1 type of "scope" in the same time.


Usage Instructions:

You can post or get data from ZohoCRM in your own module. See example below.

To post data from drupal to zoho crm, use 
zoho_v2api_post_records('Leads', $output_json, $record_id = NULL);

 @param string $zoho_module
   The api name of the module, e.g. 'Leads'.
 @param string $json
   The json data to be stored in zoho crm.
 @param string $record_id
   Optional-the Id of the record to be changed.
   If specified record already present in crm.

To fetch records from zoho crm, use
zoho_v2api_get_records($zoho_module)
 @params string
   $zoho_module: the api name of the module, e.g. 'Leads'.
 @return array
   Array of records from Zoho CRM.

JSON data example for post to the Zoho CRM for Leads module with the certain Layout:
      "data": [
        {
          "Last_Name": "Lead_NameWEB33",
          "Email": "newcrmapi2@zoho.com",
          "Description": "Design your own layouts that align.",
          "Industry": "ASP",
          "Phone": "988-3333333",
          "Street": "Address3",
          "City": "City3",
          "Lead_Source": "Advertisement3",
          "Country": "Country3",
          "Website": "www.test3.com",
          "Layout": {
             "id": "3285440000000087501",
          },
        }
       ]


Mapping fields Zoho Leads and Forms example:
      $layout_id = '3285440000000087501';
      // If don't set Layout, data will send to default Layout.
      $form_mapping = array(
        'Email' => 'field_email',
        'Last_Name' => 'field_name',
        'Phone' => 'field_phone',
        'Company' => 'field_company_name',
        'City_Of_Interest' => 'field_subtitle',
        'Start_Date' => 'field_period',
        'Input' => 'field_additional',
      );
