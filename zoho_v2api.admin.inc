<?php

/**
 * @file
 * Defines admin configuration page for the Zoho API module.
 */

/**
 * Menu callback for admin/config/zoho_v2api. Admin settings form.
 */
function zoho_v2api_admin_settings_form($form, &$form_state) {
  global $base_url;
  $form = array();
  $zoho_accounts = l(t('Zoho developer console'), 'http://accounts.zoho.com/developerconsole', array('attributes' => array('target' => '_blank')));
  $zoho_help = l(t('https://www.zoho.com/crm/help/api/v2'), 'https://www.zoho.com/crm/help/api/v2', array('attributes' => array('target' => '_blank')));
  $redirect_uri = $base_url . base_path() . request_path();
  $check_tokens = zoho_v2api_get_tokens();
  $form['authenticate_application'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Generate tokens'),
    '#collapsible' => TRUE,
    '#description' => t('Use this only for the first time to authenticate your application and to generate a new grant_token. This is a short-lived token (valid only for a minute) and will be used to generate the access token and refresh token automatically.') .
    '<br/>' . t('Before you get started with authorization, you need to register your application with Zoho CRM API.') .
    '<br/>' . t('Go to the site !zoho_accounts and add client. You will receive Client id, Client Secret, Redirect URI.', array('!zoho_accounts' => $zoho_accounts)) .
    '<br/><strong>' . t('Use this Redirect URI: @redirect_url', array('@redirect_url' => $redirect_uri)) . '</strong>' .
    '<br/>' . t('NOTE! This module version can work with only 1 type of "scope" in the same time.'),
  );
  $form['authenticate_application']['client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Client id'),
    '#default_value' => variable_get('zoho_v2api_client_id', ''),
    '#description' => t('The consumer key generated from the connected app.'),
  );
  $form['authenticate_application']['client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Client Secret'),
    '#default_value' => variable_get('zoho_v2api_client_secret', ''),
    '#description' => t('The consumer secret generated from the connected app.'),
  );
  $form['authenticate_application']['redirect_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect URI'),
    '#default_value' => variable_get('zoho_v2api_redirect_uri', $redirect_uri),
    '#description' => t('The Callback URL that you entered during the app registration.'),
  );
  $form['authenticate_application']['scope'] = array(
    '#type' => 'textfield',
    '#title' => t('Scope from Zoho CRM'),
    '#default_value' => variable_get('zoho_v2api_scope', 'ZohoCRM.modules.all'),
    '#description' => t('Please set scope for current Client ID. Each scope (and respectively temporary grant_token, 1 hour access_token and refresh_token) would be different for each client application. You can find required scope here: !zoho_help', array('!zoho_help' => $zoho_help)),
  );

  $form['access_information'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Access token information'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['access_information']['access_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Generated access_token'),
    '#default_value' => $check_tokens['access_token_value'],
    '#disabled' => TRUE,
  );
  $form['access_information']['refresh_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Generated refresh_token'),
    '#default_value' => $check_tokens['refresh_token_value'],
    '#disabled' => TRUE,
  );
  $form['access_information']['time_remained'] = array(
    '#type' => 'textfield',
    '#title' => t('Remaining time for token'),
    '#default_value' => round($check_tokens['time_remained'] / 60) . ' min',
    '#disabled' => TRUE,
  );

  $form['#validate'][] = 'zoho_v2api_validate_settings';

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Settings'),
  );
  return $form;
}

/**
 * Validate function for admin settings form.
 */
function zoho_v2api_validate_settings($form, &$form_state) {
  if (($form_state['values']['client_id'] == '') || (($form_state['values']['client_secret'] == '') || ($form_state['values']['redirect_uri'] == ''))) {
    form_set_error('', t('Please fill all fields.'));
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 *
 * Generate access_token and refresh_token if access_token time expired.
 */
function zoho_v2api_form_zoho_v2api_admin_settings_form_alter(&$form, &$form_state, $form_id) {
  // Getting current status, updating Client_id or not.
  $current_client_id = variable_get('zoho_v2api_client_id', '');
  $check_tokens = zoho_v2api_get_tokens();
  $input_client_id = !empty($form_state["input"]["client_id"]) ? $form_state["input"]["client_id"] : $current_client_id;
  if (!variable_get('zoho_v2api_update_client_status', FALSE)) {
    if ($current_client_id != $input_client_id) {
      variable_set('zoho_v2api_update_client_status', TRUE);
    }
    else {
      variable_set('zoho_v2api_update_client_status', FALSE);
      if (!empty($check_tokens['access_token_value']) &&
        $check_tokens['time_remained'] == 0) {
        watchdog('zoho_v2api', 'Current access_token expired. You need to generate new one or it can be auto-generated during data submitting to Zoho CRM.', array(), WATCHDOG_INFO);
      }
    }
  }

  // If access_token was expired we'll generate a new one.
  // after redirected back to the site with (code=)grant_token in the url.
  if (!isset($form_state["input"]["op"]) && !empty($_GET["code"])) {
    global $base_url;
    $grant_token = $_GET["code"];
    $url = ZOHO_TICKET_URL . "oauth/v2/token?code=" . $grant_token . "&redirect_uri=" . variable_get('zoho_v2api_redirect_uri') . "&client_id=" . variable_get('zoho_v2api_client_id') . "&client_secret=" . variable_get('zoho_v2api_client_secret') . "&grant_type=authorization_code";

    // Check if we want to update valid access_token or generate a new one after
    // getting a new Client ID from Zoho application.
    if (!variable_get('zoho_v2api_update_client_status', FALSE)) {
      if (!empty($check_tokens['access_token_value']) &&
        $check_tokens['time_remained'] < 0) {
        watchdog('zoho_v2api', "Current access_token is valid. We don't need to generate a new grant_token and renew access_token", array(), WATCHDOG_NOTICE);
        drupal_goto($base_url . base_path() . request_path());
      }
      elseif (!empty($check_tokens['refresh_token_value'])) {
        $refresh_token = $check_tokens['refresh_token_value'];
        $url = ZOHO_TICKET_URL . "oauth/v2/token?refresh_token=" . $refresh_token . "&client_id=" . variable_get('zoho_v2api_client_id') . "&client_secret=" . variable_get('zoho_v2api_client_secret') . "&grant_type=refresh_token";
      }
    }
    $response = drupal_http_request($url, array(
      'method' => 'POST',
      'headers' => array('Content-Type' => 'application/x-www-form-urlencoded; charset=utf-8')
    ));
    if ($response->code == "200") {
      $result = drupal_json_decode($response->data);
      $result['start_time'] = REQUEST_TIME;
      if (empty($result["error"])) {
        variable_set('zoho_v2api_tokens', $result);
        $form["access_information"]["access_token"]["#default_value"] = $result['access_token'];
        $form["access_information"]["time_remained"]["#default_value"] = round($check_tokens['time_remained'] / 60) . ' min';
        if (!empty($result['refresh_token'])) {
          // We can get refresh_token only with first access_token.
          variable_set('zoho_v2api_refresh_token', $result['refresh_token']);
        }
        watchdog('zoho_v2api', "Access_token set successfully", array(), WATCHDOG_INFO);
      }
      else {
        watchdog('zoho_v2api', 'Error getting access from Zoho CRM, please check settings of your Zoho applications: @error', array('@error' => $result["error"]), WATCHDOG_NOTICE);
      }
    }
    variable_set('zoho_v2api_update_client_status', FALSE);
    drupal_goto($base_url . base_path() . request_path());
  }


}

/**
 * Submit function for admin settings form.
 *
 * Generate grant_token.
 */
function zoho_v2api_admin_settings_form_submit($form, &$form_state) {
  // Check if we want to update valid access_token or generate a new one after
  // getting a new Client ID from Zoho application.
  variable_set('zoho_v2api_client_id', $form_state['values']['client_id']);
  variable_set('zoho_v2api_client_secret', $form_state['values']['client_secret']);
  variable_set('zoho_v2api_redirect_uri', $form_state['values']['redirect_uri']);
  variable_set('zoho_v2api_scope', $form_state['values']['scope']);
  $check_tokens = zoho_v2api_get_tokens();
  if (!variable_get('zoho_v2api_update_client_status', FALSE)) {
    if (!empty($check_tokens['access_token_value']) &&
      $check_tokens['time_remained'] < 0) {
      // Current access_token is valid. We don't need to generate a new grant_token for it.
      return;
    }
  }
  $scope = !empty($form_state['values']['scope']) ? $form_state['values']['scope'] : 'ZohoCRM.modules.all';
  $form_state['redirect'] = ZOHO_TICKET_URL . "oauth/v2/auth?scope=" . $scope . "&client_id=" . $form_state['values']['client_id'] . "&response_type=code&access_type=offline&redirect_uri=" . $form_state['values']['redirect_uri'];
}
